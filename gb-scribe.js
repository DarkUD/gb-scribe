var colors				= require("colors"),
	fs					= require("fs");

var file				= "";

/**
 * Report a status
 * @param msg
 */
function log(msg){
	console.log(" > ".green + msg.yellow);
	
	if(file !== ""){
		fs.appendFileSync(file, " > " + msg+"\n");
	}
}

/**
 * Report an error
 * @param msg
 */
function error(msg){
	console.log(" > ".red + msg.red);
	
	if(file !== ""){
		fs.appendFileSync(file, " x " + msg+"\n");
	}
}

/**
 * Append a message to the line using process.stdout
 * @param msg
 */
function apLog(msg){
	process.stdout.write(" > ".green + msg.yellow +"\r");
}

/**
 * Append a message to the line using process.stdout
 * @param msg
 */
function apError(msg){
	process.stdout.write(" > ".red + msg.red +"\r");
}

/**
 * Return infos for the app
 */
function info(){
	console.log("(gb)  Scribe v0.1".yellow);
}

/**
 * Set the active file where we will put the logs messages
 * @param file
 */
function setActiveFile(f){
	file = f;
}

/**
 * Remove the active file
 */
function removeActiveFile(){
	file = "";
}

module.exports = {
		log			: log,
		error		: error,
		info		: info,
		apLog		: apLog,
		apError		: apError,
		setFile		: setActiveFile,
		rmvFile		: removeActiveFile
};